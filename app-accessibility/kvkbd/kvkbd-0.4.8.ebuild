# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit kde

DESCRIPTION="A virtual keyboard for KDE."
HOMEPAGE="http://www.kde-apps.org/content/show.php?content=56019"
LICENSE="GPL-2"
SRC_URI="http://www.kde-apps.org/CONTENT/content-files/56019-${P}.tar.gz"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

RDEPEND=""
DEPEND="${REDPEND}"

need-kde 3
