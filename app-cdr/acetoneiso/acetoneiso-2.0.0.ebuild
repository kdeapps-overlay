# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit qt4

MY_PN="${PN}2"
MY_P="${MY_PN}_2.0.0-source"
S=${WORKDIR}/${MY_PN}/src

DESCRIPTION="A CD/DVD image manipulator for GNU/Linux."
HOMEPAGE="http://www.acetoneiso.netsons.org"
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/${MY_PN}/${MY_P}.tar.gz"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

RDEPEND="$(qt4_min_version 4.0)"
DEPEND="${REDPEND}"

src_compile()
{
	eqmake4 ${MY_PN}.pro
	emake
}

src_install()
{
	# einstall fails here
	# FIXME: l10n

	dobin acetoneiso2

	insinto /usr/share/pixmaps
	doins images/Acetino2.png

	domenu menu/AcetoneISO.desktop

	insinto /usr/share/apps/konqueror/servicemenus
	doins sm/link/*

	insinto /opt/acetoneiso
	doins sm/scripts/*

	insinto /opt/acetoneiso/dos6.0
	doins dos6.0/boot/*
	doins dos6.0/*

	dodoc ../README
	dodoc ../AUTHORS
	dodoc ../CHANGELOG
}
