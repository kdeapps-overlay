# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit qt4

DESCRIPTION="A Qt frontend for QEmu."
HOMEPAGE="http://qtemu.org/"
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/qtemu/${P}.tar.bz2"

IUSE=""
KEYWORDS="~x86"
SLOT="0"

DEPEND="$(qt4_min_version 4.1)"
RDEPEND="${DEPEND}
	app-emulation/qemu"

src_compile()
{
	eqmake4
	emake
}

src_install()
{
	dobin "qtemu"
	dodoc {ChangeLog,COPYING,README}
	dohtml help/*

	newicon images/${PN}.ico ${PN}.ico
	make_desktop_entry "qtemu" "QtEmu" "${PN}.ico" "Qt;Utility;Emulator"
}
