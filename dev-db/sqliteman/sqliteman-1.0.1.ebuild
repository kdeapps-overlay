# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit cmake-utils qt4

DESCRIPTION="Sqliteman is a simple but powerful Sqlite3 GUI database manager."
HOMEPAGE="http://sqliteman.com"
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

QT4_BUILT_WITH_USE_CHECK="sqlite3"
RDEPEND="$(qt4_min_version 4.2)
	>=dev-db/sqlite-3"
DEPEND="${REDPEND}
	dev-util/cmake"

src_compile()
{
	cmake-utils_src_compile
}
