# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

LANGS="bg br cy da el es et fr ga it ja lt nl pt rw sr sr@Latn sv tr"

inherit kde

DESCRIPTION="A boggle game for KDE, using the words from your spell checker."
HOMEPAGE="http://www.bramschoenmakers.nl/kboggle"
LICENSE="GPL-2"
SRC_URI="http://www.bramschoenmakers.nl/files/${P}.tar.bz2"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

RDEPEND=""
DEPEND="${RDEPEND}"

need-kde 3.4

pkg_setup() {
	kde_pkg_setup
	if ! built_with_use kde-base/kdelibs spell; then
		echo
		ewarn "kde-base/kdelibs was not compiled with the \"spell\" use flag."
		ewarn "This is highly recommended so that KBoggle can check the words entered during the game."
		echo
	fi
}

src_unpack() {
	kde_src_unpack
	strip-linguas ${LANGS}

	if [[ ${LINGUAS} != "" ]]; then
		LANGPO=""
		for I in ${LINGUAS}; do
			LANGPO="${LANGPO} ${I}.po"
		done

		sed -i -e "s/^POFILES =.*/POFILES =${LANGPO}/" "${S}/po/Makefile.am" || die "sed for locale failed"
		rm -f "${KDE_S}/configure"
	fi
}
