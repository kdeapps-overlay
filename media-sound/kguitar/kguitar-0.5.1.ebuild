# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

LANGS="de es fr it ru"

inherit kde

DESCRIPTION="KGuitar is a guitarist helper program focusing on tabulature editing and MIDI synthesizers support."
HOMEPAGE="http://kguitar.sf.net"
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

IUSE="kgtabs midi"
KEYWORDS="~amd64 ~x86"
SLOT="0"

RDEPEND="midi? ( >=media-libs/tse3-0.2.3 )"
DEPEND="${REDPEND}"

src_unpack()
{
	kde_src_unpack
	strip-linguas ${LANGS}

	if [[ ${LINGUAS} != "" ]]; then
		LANGPO=""
		for I in ${LINGUAS}; do
			LANGPO="${LANGPO} ${I}.po"
		done

		sed -i -e "s/^POFILES =.*/POFILES =${LANGPO}/" "${S}/po/Makefile.am" || die "sed for locale failed"
		rm -f "${KDE_S}/configure"
	fi
}

src_compile()
{
	local myconf="$(use_with kgtabs)"

	kde_src_compile
}

need-kde 3
