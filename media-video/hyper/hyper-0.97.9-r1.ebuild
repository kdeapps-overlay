# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit eutils

S=${WORKDIR}/hyperinstall

DESCRIPTION="A frontend for ffmpeg."
HOMEPAGE="http://www.kde-apps.org/content/show.php?content=67781"
LICENSE="GPL-2"
SRC_URI="http://www.kde-apps.org/CONTENT/content-files/67781-${P}.tar.gz"

IUSE="ogg"
KEYWORDS="~amd64 ~x86"
SLOT="0"

RDEPEND="|| ( kde-base/kommander kde-base/kdewebdev )
	media-video/ffmpeg
	ogg? ( media-video/ffmpeg2theora )
	|| ( media-video/mplayer media-video/mplayer-bin )"
DEPEND="${REDPEND}"

src_unpack()
{
	unpack ${A}

	sed -i -e 's|$HOME/hyper/|/usr/bin/|' ${S}/hyper.desktop
	echo "Icon=hypercon.png" >> ${S}/hyper.desktop
}

src_install()
{
	dobin hyperc.kmdr
	doicon hypercon.png
	dodoc README
	domenu hyper.desktop
	insinto /usr/share/apps/konqueror/servicemenus
	doins hyperc.desktop
}
