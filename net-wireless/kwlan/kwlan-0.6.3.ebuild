# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

LANGS="ar bg br cs da de el es et ga it ja nl pt sk sr sr@Latn sv"
# A missing Makefile.am in the doc dir, prevents us from installing localized
# docs
# LANGS_DOC="da es pt sv"
USE_KEG_PACKAGING=1

inherit kde

DESCRIPTION="A network manager for KDE."
HOMEPAGE=""
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

DEPEND="net-wireless/wpa_supplicant"
RDEPEND="${DEPEND}"

need-kde 3.3
