# KDE-Apps Gentoo Overlay
# Maintained by Bram Schoenmakers <bramschoenmakers@kde.nl>
#
# Distributed under the terms of the GNU General Public License v3
# Copyright 2008

inherit qt4

DESCRIPTION="Zhu3D is an interactive OpenGL-based mathematical function viewer."
HOMEPAGE="http://sourceforge.net/projects/zhu3d/"
LICENSE="GPL-2"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

IUSE=""
KEYWORDS="~amd64 ~x86"
SLOT="0"

QT4_BUILT_WITH_USE_CHECK="opengl"
RDEPEND="$(qt4_min_version 4.3)"
DEPEND="${REDPEND}"

src_unpack()
{
	unpack ${A}
	cd ${S}
	sed -ie 's|^SYSDIR=|SYSDIR=/usr/share/zhu3d/system|' zhu3d.pri
	sed -ie 's|^TEXDIR=|TEXDIR=/usr/share/zhu3d/textures|' zhu3d.pri
	sed -ie 's|^WORKDIR=|WORKDIR=/usr/share/zhu3d/work|' zhu3d.pri
}

src_compile()
{
	eqmake4 && emake
}

src_install()
{
	local SHAREDIR="/usr/share/${PN}/"

	dobin zhu3d

	dodoc readme.txt
	dodoc install.txt
	dodoc intel-icc.txt
	dodoc src/changelog.txt

	dodoc doc/*

	insinto /usr/share/pixmaps
	doins system/icons/${PN}.png

	insinto ${SHAREDIR}/system
	doins -r system/*

	insinto ${SHAREDIR}/work
	doins work/*.zhu

	insinto ${SHAREDIR}/textures
	doins work/textures/*

	make_desktop_entry ${PN} Zhu3D /usr/share/pixmaps/${PN}.png Math;Qt
}
